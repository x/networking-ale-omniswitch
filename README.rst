===============================
networking-ale-omniswitch
===============================

Networking-ale-omniswitch package contains Alcatel-Lucent Enterprise Omniswitch vendor code for Openstack Neutron.

* Free software: Apache license
* Documentation: https://wiki.openstack.org/wiki/Neutron/ML2/ALE-Omniswitch
* Source: http://git.openstack.org/cgit/openstack/networking-ale-omniswitch
* Bugs: http://bugs.launchpad.net/networking-ale-omniswitch

Getting started
---------------

* Please visit: https://wiki.openstack.org/wiki/Neutron/ML2/ALE-Omniswitch
