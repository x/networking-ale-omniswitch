============
Installation
============

At the command line::

    $ pip install networking-ale-omniswitch

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv networking-ale-omniswitch
    $ pip install networking-ale-omniswitch
